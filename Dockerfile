# Download base image ubuntu 20.04

FROM ubuntu:22.04

# INFO

LABEL maintainer="peixoto.leopoldo@outlook.com"
LABEL version="0.3"
LABEL description = "This is custom Docker Image for Leopoldo"

# Disable Prompt During Packages Instalation

ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository

RUN apt update

# Install network packages

RUN apt install -y net-tools curl awscli

# Install vim

RUN apt install -y vim